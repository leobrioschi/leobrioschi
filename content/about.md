+++
title = "Leonardo Brioschi"
subtitle = "Ph.D. student in Finance/Accounting"
author = "Leonardo Brioschi"
tags = ["about"]
+++

A curious mind, I graduated in Economics ( **2013** ) went on to the private sector but during my last position ( **FP&A** ) my desire to expand my comprehension was the drive to enroll in my PhD. My [research]({{< ref "research.md" >}}) currently involves answering *how politicians affect the financial markets*.

Besides that, my enjoyment with all things *econometrics* and *data* means that I like learning and doing **Data Science**, **Applied Math** and **Time-Series Forecast**.

> Download my [**Curriculum Vitae**](/pdf/cv.pdf)

Get in touch with [leobrioschi (at) proton.me](mailto:leobrioschi@proton.me)