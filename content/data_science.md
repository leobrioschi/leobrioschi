+++
title = "Data Science"
subtitle = "Projects and repos"
+++

I have a few projects publicly available at my [gitlab](https://gitlab.com/leobrioschi), in no particular order:

1. [**AutoRegressive Choir of Ibovespa (Python)**](https://leobrioschi.gitlab.io/autoregressive-choir-of-ibovespa/README.html):
A trading strategy based on buying the best 10 stocks at the opening of the market and selling at Close. It forecasts daily returns by fitting a *SARMA(4,0)m(1,0,0,15)* for a list of 70 stocks from the index and then testing with real data.
  
2. [**B3 Historical Quotes_Options parser (Python)**](https://leobrioschi.gitlab.io/b3-historical-quotes-options-parser):
A parser for the historical quotes file available at *B3's website*, done mainly because I wanted daily data for stock options in Brazil but couldn't get anywhere.

3. [**Logistic Regression with Numpy/Numba/JAX (Python)**](https://leobrioschi.gitlab.io/logit-with-numpy-numba-jax/):
I tried to naively optimize a classic LR(MLE) with Gradient Descent testing with Numpy, Numba and JAX.JAX is easy to use and very fast.

4. [**Oil price modelling with VAR and trading strategy (Python)**](https://gitlab.com/leobrioschi/oil-prices-modelling-with-var-and-trading-strategy):
A trading strategy based on VAR model for Oil prices (*FRED rates and Crude Oil prices*). After fitting the model, I set up the strategy based on the deviation bands and next day forecast. 

5. [**Modelling customer Churn (Python)**](https://gitlab.com/leobrioschi/modeling-customer-churn):
I used *Gradient boosting classification* to model client churn predictions over a publicly available Kaggle database. 
  
  **More at [*GitLab*](https://gitlab.com/leobrioschi)**
